function preload() {

}

function setup() {
    var cnv = createCanvas(windowWidth, windowHeight);
    cnv.style('display', 'block');
    
  
    
    displayDensity(1)
    pixelDensity(1)
   
    background('#313236')
    //noSmooth()
    textSize(20)
}

var MechaLogo = [23,159,49,203,49,97,23,89]
var MechaLogo2 = [24,55,77,136,78,97 ,50,58]
var MechaLogoVQuadOld = [
                        [23,159,49,203,49,97,23,89],
                        [24,55,77,136,78,97 ,50,58],
                        [77,136,109,105,110,63,78,97],
                        [110,63,148,66,148,199,110,229]
                    ]

var MechaLogoVQuad = [
    [1,15,39,8,38,174,1,144],
    [39,8,40,50,72,81,71,42],
    [72,81,72,42,99,3,125,0],
    [100,42,126,34,126,104,100,148]
]


function vQuad(vQuadArr =[],x=0,y=0,Xscale=1,Yscale=1){
    push()
    scale(Xscale, Yscale)
    translate(x, y)
    for(var i = 0;i < vQuadArr.length;i++){
        quad(vQuadArr[i][0],vQuadArr[i][1],vQuadArr[i][2],vQuadArr[i][3],vQuadArr[i][4],vQuadArr[i][5],vQuadArr[i][6],vQuadArr[i][7])
    }
    pop()
}

function draw() {
    background('#313236')


    fill('#0fff8')
    //ellipse(width/2,height/2,50,50)
    //quad(MechaLogo[0],MechaLogo[1],MechaLogo[2],MechaLogo[3],MechaLogo[4],MechaLogo[5],MechaLogo[6],MechaLogo[7])
    
    //push()
    //translate(frameCount*2 % (width + 127),frameCount*2 % (height +175))
    //translate(0,-175)
    //vQuad(MechaLogoVQuad, 0,0,1,1)
    //vQuad(MechaLogoVQuad,0,0,-1,1)
    //pop()
    push()
    translate(width/2 -127/4,height/2-175/2)
    vQuad(MechaLogoVQuad, 0,0,1,1)
    vQuad(MechaLogoVQuad,0,0,-1,1)
    pop()
    //ellipse(width/3,height/3,50,50)
}