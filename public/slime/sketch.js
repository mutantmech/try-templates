//code relateing to food is in food.js
//code relating to slime is in slime.js

var foodArray = new Array()
var slimeArray = new Array()


function setup() {  
  //My screen is high pixel density to pass dim checks on the png i have to set the pixel density
  pixelDensity(1)
  // create the canvas (800px wide, 800px high)
  createCanvas(800, 800);

  // draw a border to help you see the size
  // this isn't compulsory (remove this code if you like)
  strokeWeight(5);
  rect(0, 0, width, height);

  //This function and functions relating to slime are found in slime.js
  slimeArray = new Array()
  
  slimeArray.push(new slime('#3f78'))
  slimeArray.push(new slime('#2f88'))
  
  //slimeArray.push(new slime('#3f78'))
}


//[3] function addapted from 2D collision detection book by Thomas Schwarzal
function vectorLength(x,y){
  return sqrt(x*x + y*y)
}

//[6]helper function for converting radians to normalized cartasian
function radToXY(radian){
  return {x: Math.cos(radian),    y: Math.sin(radian)};
}

function drawBackground(){
  fill(256,256,5,128)
  ellipse(width/2,width/2 ,width/1.5 *1.1,width/3 *1.1)
  fill(200,200,200,128)
  stroke(200,200,200,128)
  ellipse(width/2,width/2,width/1.5 *1.2,width/3 *1.2)
  fill(1,1,1,0)
  for(var i = 0; i < 5;i++)
    ellipse(width/2,width/2 - i *10,width/1.5 *1.2,width/3 *1.2)
}

function randomPointOnEllipse(X,Y, width,height){
  var angle = random(-PI,PI)
  var distanceX = random (-width/2,width/2)
  var distanceY = random (-height/2,height/2)
  var pos = radToXY(angle) 
  //position will be bound to ellipse
  return {x: (pos.x * distanceX) + X, y: (pos.y * distanceY) + Y }
}

function draw() {  
  //Controll the food array
  if(foodArray.length < 50 )//no more then 50 foods
    if(frameCount%50 == 0||foodArray.length<25)//limit food production
      foodArray.push(new Food())//create new food

  background(256)
  
  drawBackground()//draw the agar culture background

  // your cool monster code goes in this draw function
  for(var i =0; i < slimeArray.length;i++)
    slimeArray[i].body.update()//slimes seek food
  
  stroke('#0000')

    
  //text('Food num'+ foodArray.length,300,120)//keep track of food supply
  for(var i =0; i< foodArray.length;i++)
    foodArray[i].draw()//draw all food

  for(var i =0;i< slimeArray.length;i++)
    slimeArray[i].body.drawRecursive()//draw the slimes
}

// when you hit the spacebar, what's currently on the canvas will be saved (as a
// "monster.png" file) to your downloads folder
function keyTyped() {
  if (key === " ") {
    saveCanvas("monster.png");
  }
}
