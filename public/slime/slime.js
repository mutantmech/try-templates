let testString = "Correctly linked";


class slime{
  body
  head
  eyeLid1
  eyeLid2
  nostral1
  nostral2
  eye1
  eye2
  pupil1
  pupil2

  constructor(slimecolor){
    //define the slimes hiaracy
    var Point = randomPointOnEllipse(width/2,width/2 ,width/1.5,width/3)
    this.body = new cShape(Point.x,Point.y,100,55,0,0)
    this.head = new cShape(0,-25,80,70,0,12.5).add(this.body)
    this.eyeLid1 = new cShape(0,-20,30,30,-PI/3,30).add(this.head)
    this.eyeLid2 = new cShape(0,-20,30,30,PI/3,30).add(this.head)
    this.nostral1 = new cShape(0,3,2,2,PI/10,40).add(this.head)
    this.nostral2 = new cShape(0,3,2,2,-PI/10,40).add(this.head)
    this.eye1 = new cShape(0,5,25,25,PI/10,5).add(this.eyeLid1)
    this.eye2 = new cShape(0,5,25,25,-PI/10,5).add(this.eyeLid2)
    this.pupil1 = new cShape(0,0,5,15,PI/4,12.5).add(this.eye1)
    this.pupil2 = new cShape(0,0,5,15,-PI/4,12.5).add(this.eye2)
  
    var invisible = '#0000'
     
    var eyeColor = '#fcc'
    this.body.c = slimecolor
    this.head.c = slimecolor
    this.eyeLid1.c = slimecolor
    this.eyeLid2.c = slimecolor
    this.eye1.c = eyeColor
    this.eye2.c = eyeColor
  }

}




//slimes love dots
class Food{
  x
  y
  mass
  c
  targeted

  constructor(){
      var Point = randomPointOnEllipse(width/2,width/2 ,width/1.5,width/3)
      this.x = Point.x
      this.y = Point.y
      this.mass = random(5,15)
      this.c = '#0c0f'
      this.targeted = false
  }

  //function for flagging the food as next target of a slime
  setAsTarget(other = cShape){
      if(this.targeted == false){
      other.target = this
      this.targeted = true
      }
  }

  draw(){
      fill(this.c)
      ellipse(this.x,this.y,this.mass,this.mass)
  }
}


class cShape{
  //offsets
  x
  y
  yOffset = 0
  //width and height of body peice
  w
  h
  //color plan to randomize or have it modified by food color
  c = '#f0f'
  //the angle relative to parant
  aX =0
  //the distance from parent along angle
  D
  //hirarchy
  parent
  children = new Array()  
  
  //the object the slime will seek towards
  target
  eating = false

  constructor(x,y,w,h,aX=0,D=0){
    this.w = w
    this.h = h
    this.D = D
    this.aX = aX
    this.x = x
    this.y = y 
  }
  
  //set a parent child relationship
  add(other = cShape){
    this.parent = other
    other.children.push(this)
    return this
  }

  //loop thru food decide on food to eat
  pickTarget(arr = new Array(Food)){
    var closest =0
    var Closer = 1000
    var clear = false
    for(var i=0; i < arr.length;i++){
      //ranked value based on distance
      var dist = abs( vectorLength(this.x - arr[i].x , this.y - arr[i].y) )
        
      if(arr[i].targeted == false)
        if(dist < Closer){//keep track of food with lowest distance
          Closer = dist
          closest = i
          clear = true
        }      
    }
    if(clear)
      arr[closest].setAsTarget(this)
  }
  
  
  //recurses thru parents to get the absolute rotation of a part
  getTurn(){
    var Turn = this.aX
    if(this.parent != null)
      Turn = this.aX + this.parent.getTurn()
  
    //I had to fudge the numbers abit to keep them within range of -PI <> PI
    Turn = ((Turn+PI) %TWO_PI)-PI 
  
    return Turn
  }
  


  update(){
    if(this.eating)
      this.eat()
    else
      this.seek()

  }

  eatPhrases = ["YUM !","NUM !"]
  Phrase = 0
  nomCount = 0
  eat(){//nom nom
    if(this.nomCount > 20){//return to roaming
      this.eating = false;
      this.nomCount = 0;   
      this.children[0].yOffset = 0
    }
    
    fill(128,128,128)
    text(this.eatPhrases[this.Phrase],this.x-25,this.y-100 - this.children[0].yOffset)//exclaim NOM!!
    this.children[0].yOffset = sin(frameCount/3 % TWO_PI) *5
    this.nomCount++
  }
  
  seek(){
    if(this.target == null)
      this.pickTarget(foodArray)
    else{
      var X = this.target.x
      var Y = this.target.y
      //[5] function found on stack overflow
      var angle = atan2(Y - this.y, X - this.x ) //convert from cartasian to radian   
  
      //this is the distance between current angle and desired angle
      var turn = (this.aX) - (angle+PI/2) 
      
      this.aX -= turn //modify angle
  
      var steer = radToXY(this.aX - PI/2) //return the new angle to cartasian

      
      if(abs(this.x - X) > 5 || abs(this.y - Y) >5){//target is further then 5 units
        //move towards target
        this.x += steer.x *2
        this.y += steer.y *2
      }else if(abs(this.x - X) < 5 && abs(this.y - Y) <5){//target is closer then 5 units
        for(var i = 0; i< foodArray.length;i++){  
          //consume target
          if(this.target!=null)
            if(foodArray[i] == this.target){//find target in array  
              //[2] This was found on stack overflow
              foodArray.splice(i,1)//remove target from array
              delete this.target//hopefully prevents data leaks
              this.target = null
              this.eating = true
              this.Phrase = Math.round(random(0,this.eatPhrases.length-1))
            }
        }
      }
      if(this.target == null)
        this.pickTarget(foodArray);//pick new target
    }
  }
  
  //a sort function for decideing draw order //may be better to store this value each frame
  sortByTurn(a,b){
    return((a.getTurn()) - (b.getTurn()))
  }
  
  //get the screen X pos
  getX(){
    var X = this.x //root object only outputs X
    if(this.parent !=null) 
      X = this.parent.getX() + sin(this.getTurn())*this.D + this.x; //this is that satisfing turning effect
    return X
  }
  
  getY(){
    if(this.parent != null){
      var Y = this.parent.getY() + sin(this.getTurn()-PI/2)*this.D/3 + this.y +this.yOffset // similar for Y
      return Y
    }
    else
      return this.y
  }
  
  //unused now but still handy
  //this recurses to grab all objects that decend from objects children
  Decendants(arr = new Array()){
    arr.push(this)
    for(var i =0; i < this.children.length;i++){
      arr = this.children[i].Decendants(arr);
    }
    return arr
  }
  

  //draw the actual ellipse
  drawEllipse(){      
    //get screen Pos
    var X = this.getX()
    var Y = this.getY() 

    fill(this.c)
    ellipse(X,Y,this.w,this.h)
  }

  //recursively draw creature taking acount of draworder
  drawRecursive(){
    //sort the list for cleaner draw order
    this.children.sort(function(a,b){return a.sortByTurn(a,b)})
  
    //draw children behind first
    for(var i =0; i < this.children.length;i++){
      if(abs(this.children[i].getTurn())< PI/2 )
        this.children[i].drawRecursive()
    }
  
    //draw the actual body part
    this.drawEllipse()
      
    //draw children in front last
    for(var i =0; i < this.children.length;i++){
      if(abs(this.children[i].getTurn())>= PI/2 )
        this.children[i].drawRecursive()
    }    
  }
}
  