class Animation{
    sequence = []
    fps = 0
    T = 0

    frame =0
    constructor(sequence =[],fps = 30){
        this.sequence = sequence
        this.fps = fps
    }

    lastUpdateFrame=-1
    update() {
        if(this.lastUpdateFrame != frameCount){
            this.T += deltaTime *0.001
            this.frame = this.sequence[ Math.trunc(this.T *this.fps)%this.sequence.length ] 
            console.log(""+this.frame)
        }
        this.lastUpdateFrame = frameCount
    }
}


const FLIP_None = 0
const FLIP_X = 1
const FLIP_Y = 2
const FLIP_XY = 3

class Sprite{
    IMG = image
    FrameX
    FrameY
    FrameTotal

    W
    H

    
    constructor(img,FrameX,FrameY){
        this.IMG = img
        this.FrameX = FrameX
        this.FrameY = FrameY
        this.FrameTotal = FrameX * FrameY 
    }

    drawAni(x,y,Ani,flip = 0){
        Ani.update()
        this.draw(x,y,Ani.frame,flip)
    }

    draw(x,y, Frame,flip = 0){
        x = Math.trunc(x) //only draw at whole coodinates
        y = Math.trunc(y)

        Frame = Frame % this.FrameTotal 

        var FX = Frame % this.FrameX
        var FY = floor( Frame / this.FrameX )
        
        this.W = this.IMG.width/this.FrameX
        this.H = this.IMG.height/this.FrameY


        
        push()       
        //flipX
        if(flip == 1 || flip == 3){
            translate(x + this.W,0)
            scale(-1,1)
        }else{
            translate(x,0)
        }
        
        //flipY
        if(flip == 2 || flip == 3){
            translate(0,y + this.H)
            scale(1,-1)
        }else{
            translate(0,y)
        }

        image(this.IMG,0, 0, this.W, this.H,
            FX * this.W,FY * this.H,this.W,this.H)
            
        pop()
    }
}